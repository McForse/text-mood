package org.example;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;

public class MoodAnalyzer {
    private static final Logger logger = Logger.getLogger(MoodAnalyzer.class.getName());
    private final Set<String> positiveWords;
    private final Set<String> negativeWords;

    public MoodAnalyzer(Set<String> positiveWords, Set<String> negativeWords) {
        this.positiveWords = positiveWords;
        this.negativeWords = negativeWords;
    }

    public int calculateMoodIndex(String text) {
        String[] words = text.toLowerCase().replaceAll("[^а-яёА-ЯЁ\\s]", "").split("\\s+");
        int positiveCount = 0;
        int negativeCount = 0;

        for (String word : words) {
            if (positiveWords.contains(word)) {
                positiveCount++;
            }
            if (negativeWords.contains(word)) {
                negativeCount++;
            }
        }

        return positiveCount - negativeCount;
    }

    public static void main(String[] args) {
        try {
            String text = new String(Files.readAllBytes(Paths.get("text.txt")));
            Set<String> positiveWords = loadWords("positive_words.txt");
            Set<String> negativeWords = loadWords("negative_words.txt");
            MoodAnalyzer moodAnalyzer = new MoodAnalyzer(positiveWords, negativeWords);

            int moodIndex = moodAnalyzer.calculateMoodIndex(text);
            logger.info("Индекс настроения: " + moodIndex);
        } catch (IOException e) {
            logger.info("Ошибка при чтении файлов: " + e.getMessage());
        }
    }

    private static Set<String> loadWords(String filename) throws IOException {
        return new HashSet<>(Files.readAllLines(Paths.get(filename)));
    }
}