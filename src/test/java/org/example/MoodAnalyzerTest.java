package org.example;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

class MoodAnalyzerTest {
    private final Set<String> positiveWords = new HashSet<>(List.of("хороший", "прекрасный", "радость"));
    private final Set<String> negativeWords = new HashSet<>(List.of("плохой", "ужасный", "неприятный", "грусть"));
    private MoodAnalyzer underTest = new MoodAnalyzer(positiveWords, negativeWords);

    @Test
    @DisplayName("Индекс настроения должен быть положительным")
    void calculateMoodIndex_shouldBePositive() {
        String text = "Хороший радость грусть";
        int moodIndex = underTest.calculateMoodIndex(text);
        assertEquals(moodIndex, 1);
    }

    @Test
    @DisplayName("Индекс настроения должен быть отрицательным")
    void calculateMoodIndex_shouldBeNegative() {
        String text = "Хороший ужасный грусть";
        int moodIndex = underTest.calculateMoodIndex(text);
        assertEquals(moodIndex, -1);
    }
}